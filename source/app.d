import core.thread : Thread;
import std.concurrency;
import std.datetime;
import std.file;
import std.getopt;
import std.process;
import std.stdio;

string usage() pure
{
    return "Usage: lazarus [-i n] <program> [args...] -- <main dependency> [dependencies...]";
}

void watch(Tid parentTid, size_t seconds, immutable string[] files)
{
    auto times = new SysTime[files.length];

    for (size_t i; i < times.length; i++)
        times[i] = files[i].timeLastModified();

    size_t i;

    while (true)
    {
        auto modified = files[i].timeLastModified();

        if (times[i] < modified)
        {
            parentTid.send(true);
            times[i] = modified;
        }

        if (files.length <= ++i)
            i = 0;

        Thread.sleep(dur!("seconds")(seconds));
    }
}

void main(string[] args)
{
    size_t split;
    size_t interval = 2;

    auto opts = getopt(args, config.keepEndOfOptions, config.stopOnFirstNonOption,
        "interval|i", "Number of seconds to wait before checking files", &interval);

    if (opts.helpWanted)
    {
        defaultGetoptPrinter(usage(), opts.options);
        return;
    }

    for (size_t i = 1; i < args.length; i++)
    {
        if (args[i] == "--")
        {
            split = i;
            break;
        }
    }

    if (split == 0)
    {
        usage().writeln();
        return;
    }

    immutable program = args[1..split].dup;
    immutable files = args[split + 1..$].dup;

    foreach (file; files)
    {
        if (!file.exists()) {
            writefln("File '%s' does not exist", file);
            return;
        }
    }

    auto watcherTid = spawn(&watch, thisTid, interval, files);
    auto pid = spawnProcess(program);

    while (true)
    {
        if (receiveOnly!bool)
        {
            writefln("Reloading %s...", program[0]);
            kill(pid);
            wait(pid);
            pid = spawnProcess(program);
        }
    }
}
